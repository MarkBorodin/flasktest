import datetime
import random
import string

from faker import Faker


def parse_length(request, default=10):
    length = request.args.get('length', str(default))

    if not length.isnumeric():
        raise ValueError("VALUE ERROR: int")

    length = int(length)

    if not 3 < length < 100:
        raise ValueError("RANGE ERROR: [3..10]")

    return length


def gen_password(length):
    result = ''.join([
        str(random.randint(0, 9))
        for _ in range(length)
    ])
    return result


def gen_password_with_chars(length):
    CHARS_LEN = 3
    result = gen_password(length - CHARS_LEN) + ''.join([
        str(random.choice(string.ascii_letters))
        for _ in range(CHARS_LEN)
    ])
    return result


def generate_current_time():
    return datetime.datetime.now()


def format_list(lst):
    return '<br>'.join(str(elem) for elem in lst)


def parse_count(count):

    if not count.isnumeric():
        raise ValueError("VALUE ERROR: int")

    count = int(count)

    if not 0 < count <= 100:
        raise ValueError("RANGE ERROR: [1..100]")

    return count


def gen_random_customer():
    fake = Faker()
    customer = {
        'first_name': fake.first_name(),
        'last_name': fake.last_name(),
        'email': fake.email(),
        'phone_number': fake.phone_number(),
        'city': fake.city(),
        'country': fake.country(),
        'street_address': fake.street_address(),
        'postcode': fake.postcode(),
        'SupportRepId': str(random.randint(1, 8))
    }
    return customer
