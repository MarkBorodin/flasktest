import json
import requests

from flask import Flask, request, Response
from utils import gen_password, parse_length, gen_password_with_chars, format_list, parse_count, gen_random_customer
from db import execute_query, get_customers_by_name, get_customers_by_place, add_customer


app = Flask(__name__)


@app.route('/')
def hello_world():
    return 'Hello World!'


@app.route('/now')
def get_current_time():
    return str(get_current_time())


@app.route('/password')
def get_random():
    chars = request.args.get('chars', 0)
    try:
        length = parse_length(request, 10)
    except Exception as ex:
        return Response(str(ex), status=400)

    if chars == '1':
        return gen_password_with_chars(length)
    else:
        return gen_password(length)


@app.route('/bitcoin')
def get_bitcoin_rate():
    currency = request.args.get('currency', 'USD')
    response = requests.get(
        url='https://bitpay.com/api/rates'
    )
    rates = json.loads(response.text)
    for rate in rates:
        if rate['code'] == currency:
            return str(rate['rate'])
    return 'N/A'


@app.route('/customers')
def get_customers():
    first_name = request.args.get('first_name')
    last_name = request.args.get('last_name')
    city = request.args.get('city')
    country = request.args.get('country')
    if city or country:
        records = get_customers_by_place(city, country)
        return format_list(records)
    else:
        records = get_customers_by_name(first_name, last_name)
        return format_list(records)


@app.route('/unique_names')
def get_unique_names():
    records = execute_query('SELECT FirstName FROM Customers')
    return str(len(set(records)))


@app.route('/tracks_count')
def get_tracks_count():
    records = execute_query('SELECT * FROM Tracks')
    return str(len(records))


@app.route('/gross_turnover')
def get_gross_turnover():
    records = execute_query('SELECT UnitPrice, Quantity FROM Invoice_Items')
    result = 0
    for record in records:
        result += record[0] * record[1]
    return str(result)


@app.route('/genres')
def get_genres():
    result = execute_query('select sum(t.Milliseconds)/(1000) '
                           'as duration_sec, g.Name from tracks as t '
                           'inner join genres as g '
                           'on t.GenreId = g.GenreId '
                           'group by g.GenreId')
    return format_list(result)


@app.route('/generate_customer')
def generate_customer():
    count = request.args.get('count', '1')
    try:
        count = parse_count(count)
    except Exception as ex:
        return Response(str(ex), status=400)

    for i in range(count):
        customer = gen_random_customer()
        add_customer(customer)
    return f'added {count} customers' if count > 1 else 'added one customer'


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8003)
