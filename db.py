import os
import sqlite3


def execute_query(query, *args):
    db_path = os.path.join(os.getcwd(), 'chinook.db')
    conn = sqlite3.connect(db_path)
    cur = conn.cursor()
    cur.execute(query, args)
    records = cur.fetchall()
    return records


def execute_query_for_customers(query, args):
    db_path = os.path.join(os.getcwd(), 'chinook.db')
    conn = sqlite3.connect(db_path)
    cur = conn.cursor()
    cur.execute(query, args)
    records = cur.fetchall()
    return records


def get_customers_by_name(first_name, last_name):
    sql_query = 'SELECT FirstName, LastName FROM Customers'
    filters = []
    params = []
    if first_name or last_name:
        sql_query = sql_query + ' WHERE '

    if first_name:
        filters.append(f'FirstName=?')
        params.append(first_name)

    if last_name:
        filters.append(f'LastName=?')
        params.append(last_name)

    if filters:
        sql_query = sql_query + ' AND '.join(filters)

    records = execute_query_for_customers(sql_query, params)
    return records


def get_customers_by_place(city, country):
    sql_query = 'SELECT City, Country FROM Customers'
    filters = []
    params = []
    if city or country:
        sql_query = sql_query + ' WHERE '

    if city:
        filters.append(f'City=?')
        params.append(city)

    if country:
        filters.append(f'Country=?')
        params.append(country)

    if filters:
        sql_query = sql_query + ' AND '.join(filters)

    records = execute_query_for_customers(sql_query, params)
    return records


def add_customer(customer):
    db_path = os.path.join(os.getcwd(), 'chinook.db')
    conn = sqlite3.connect(db_path)
    cur = conn.cursor()
    cur.execute("INSERT INTO customers (FirstName, LastName, Email, Phone, City, Country, Address,"
                " PostalCode, SupportRepId) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                (
                    customer['first_name'], customer['last_name'], customer['email'],
                    customer['phone_number'], customer['city'], customer['country'],
                    customer['street_address'], customer['postcode'], customer['SupportRepId'])
                )
    conn.commit()
    conn.close()
